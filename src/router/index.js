import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    name: 'start',
    path: '/start',
    component: () => import(/* webpackChunkName: "views" */'../views/Start.vue')
  },
  {
    name: 'explain',
    path: '/help',
    component: () => import(/* webpackChunkName: "views" */'../views/Explain.vue')
  },
  {
    name: '404',
    path: '/404',
    component: () => import(/* webpackChunkName: "views" */'../views/404.vue')
  },
  {
    name: 'words',
    path: '/play_as/:playerName/:sentence?',
    props: true,
    component: () => import(/* webpackChunkName: "views" */'../views/Words.vue')
  },
  {
    name: 'finish',
    path: '/finish/:playerName/:sentence',
    props: true,
    component: () => import(/* webpackChunkName: "views" */'../views/Finish.vue')
  },
  {
    path: '/',
    redirect: {
      name: 'start'
    }
  },
  {
    path: '*',
    redirect: {
      name: '404'
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
