module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/silly-sentences/' : '/',
  assetsDir: 'z',
  pwa: {
    name: 'Silly Sentences',
    themeColor: '#cdcdcd',
    appleMobileWebAppStatusBarStyle: 'black'
  },
  chainWebpack: config => {
    config.plugin('html')
      .tap(args => {
        args[0].title = 'Silly Sentences'
        return args
      })
  }
}